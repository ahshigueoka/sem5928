% 
% Inclus�o de Integradores - veja arquivo Aula8_ExemploLQG.doc
%
% SEM 5928 - Sistemas de Controle 
% Escola de Engenharia de S�o Carlos - USP
%
% Adriano Siqueira - 2016
%

clear;
Ap=[-0.02 0.005 2.4 -32;
    -0.014 0.44 -1.3 -30; 
     0 0.018 -6 1.2;
     0 0 1 0];
Bp=[0.14 -0.12;
   0.36 -8.6;
   0.35 0.009;
   0 0];
Cp=[0 1 0 0;
   0 0 0 57.3];
Dp=[0 0; 0 0];

sysGp=ss(Ap,Bp,Cp,Dp);

figure
sigma(sysGp)
grid

A=[ 0 0 0 0 0 0;
   0 0 0 0 0 0;
   0.14 -0.12 -0.02 0.005 2.4 -32;
   0.36 -8.6 -0.014 0.44 -1.3 -30; 
   0.35 0.009 0 0.018 -6 1.2;
   0 0 0 0 1 0];
B=[1 0;
   0 1;
   0 0;
   0 0;
   0 0;
 	0 0];
C=[0 0 0 1 0 0;
   0 0 0 0 0 57.3];
D=[0 0; 0 0];

sysG=ss(A,B,C,D);

figure
sigma(sysG)
grid
