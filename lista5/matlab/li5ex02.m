close all
clear
clc

format shorte

%% Model definition
A = [ -0.02, 0.005,  2.4, -32;...
     -0.014,  0.44, -1.3, -30;...
          0, 0.018,   -6, 1.2;...
          0,     0,    1,   0];
B = [0.14, -0.12;
     0.36,  -8.6;
     0.35, 0.009;
        0,     0];
    
C = [0, 1, 0,    0;...
     0, 0, 0, 57.3];
 
D = [0, 0;...
     0, 0];
 
pc = [-3+3j, -3-3j, -3, -4];

%% Controller design
K = place(A, B, pc);

[V, polescl] = eig(A-B*K);

fid = fopen('li5ex02.txt', 'w');

fprintf(fid, 'Feedback gain matrix K for controller:\n');
fprintf(fid, [repmat('%10.5g', 1, 4), '\n'], K);
fprintf(fid, '\nClosed loop poles system after feedback with K:\n');
fprintf(fid, '    %e\n', diag(polescl));

% Closed loop system in state-space representation
syscl = ss(A-B*K, B, C, D);

num_samples = 1000;
t_vec = linspace(0, 3, num_samples);
[yc, ~, x] = initial(syscl, [0, 1, 0, 30]', t_vec);

whitein = random('norm', 0, 1, num_samples, 2);
[yn, ~, xn] = lsim(syscl, whitein, t_vec, [0, 1, 0, 30]');

fig1h = figure('Name', 'Complete state feedback, y', ...
        'Position', [50, 50, 400, 300], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 9], ...
        'PaperPosition', [0, 0, 12, 9], 'PaperPositionMode', 'manual');
sub1h = subplot(2, 1, 1);
plot(t_vec, yc(:, 1), t_vec, yn(:, 1))
xlabel('Time (s)')
ylabel('Output y_1')
legend({'y_{1}', 'y_{n1}'})
sub1h = subplot(2, 1, 2);
plot(t_vec, yc(:, 2), t_vec, yn(:, 2))
xlabel('Time (s)')
ylabel('Output y_2')
legend({'y_{2}', 'y_{n2}'})

fig2h = figure('Name', 'Complete state feedback, x',...
    'Position', [50, 500, 400, 300], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 9], ...
        'PaperPosition', [0, 0, 12, 9], 'PaperPositionMode', 'manual');
plot(t_vec, x);
grid on

%% Observer design
po = [-15+15j, -15-15j, -15, -20];
L = place(A', C', po)';

[V, polescl] = eig(A-L*C);
fprintf(fid, '\nObserver poles:\n');
fprintf(fid, '%10.5g & %10.5g\\\\\n', L);

%% Results comparison
% Compare the results of using complete state feedback and estimated state
% feedback

% Closed-loop system using estimated state feedback
Aocl = [A, -B*K; L*C, A-B*K-L*C];
Bocl = zeros(8, 1);
Cocl = [          C, zeros(2, 4);...
        zeros(2, 4),          C];
    
Docl = zeros(4, 1);

sysocl = ss(Aocl, Bocl, Cocl, Docl);

z0 = [0; 1; 0; 30; 0; 0; 0; 0];

[yoc, t, x] = initial(sysocl, z0, t_vec);

fig3h = figure('Name', 'Observer-controller, output y1', ...
    'Position', [450, 50, 400, 300], ...
    'PaperUnits', 'centimeters', 'PaperSize', [12, 9], ...
    'PaperPosition', [0, 0, 12, 9], 'PaperPositionMode', 'manual');
plot(t, yc(:, 1), t, yoc(:, 1), t, yoc(:, 3), '--');
grid on
legend({'$y_{S1}$', '$y_1$', '$\hat{y}_1$'}, 'Interpreter', 'latex')

fig4h = figure('Name', 'Observer-controller, output y2',...
    'Position', [450, 500, 400, 300], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 9], ...
        'PaperPosition', [0, 0, 12, 9], 'PaperPositionMode', 'manual');
plot(t, yc(:, 2), t, yoc(:, 2), t, yoc(:, 4), '--');
grid on
legend({'$y_{S2}$', '$y_2$', '$\hat{y}_2$'}, 'Interpreter', 'latex')

fig5h = figure('Name', 'State space for plant and observer', ...
    'Position', [850, 50, 600, 800], ...
    'PaperUnits', 'centimeters', 'PaperSize', [12, 16], ...
    'PaperPosition', [0, 0, 12, 16], 'PaperPositionMode', 'manual');
subplot(4, 1, 1)
plot(t, x(:, [1, 5]))
legend({'sem obs', 'com obs'}, 'Location', 'SouthEast')
grid on
subplot(4, 1, 2)
plot(t, x(:, [2, 6]))
grid on
subplot(4, 1, 3)
plot(t, x(:, [3, 7]))
grid on
subplot(4, 1, 4)
plot(t, x(:, [4, 8]))
grid on

%print(fig1h, '-dpdf', 'li5ex02yc.pdf')
print(fig1h, '-dpng', '-r300', 'li5ex02ys.png')

%print(fig2h, '-dpdf', 'li5ex02Xc.pdf');
print(fig2h, '-dpng', '-r300', 'li5ex02Xs.png');

%print(fig3h, '-dpdf', 'li5ex02y1oc.pdf');
print(fig3h, '-dpng', '-r300', 'li5ex02y1oc.png');

%print(fig4h, '-dpdf', 'li5ex02y2oc.pdf');
print(fig4h, '-dpng', '-r300', 'li5ex02y2oc.png');

%print(fig5h, '-dpdf', 'li5ex02Xoc.pdf')
print(fig5h, '-dpng', '-r300', 'li5ex02Xoc.png')

fclose(fid);
