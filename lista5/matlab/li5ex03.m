close all
clear
clc

Nx = 2;
Ny = 2;
Nu = 2;

Ap = [-3.488,  1.169; ...
     -0.597, -1.847];
Bp = [0.0655, 4.3839; ...
      0.0755, 0.9098];
Cp = [1.0, 0.0;...
      0.0, 1.0];
  
A = [zeros(Nu, Nu+Nx);...
           Bp,    Ap];
B = [eye(Nu); zeros(Nx, Nu)];
C = [zeros(Ny, Nu), Cp];
D = zeros(Ny, Nu);

H = [-4.74880, 33.48100;...
      0.88541, -0.70226;...
      0.90584,  0.00000;...
      0.00000,  0.8344];

w_span = [1e-2, 1e3];
sysaug = ss(A, B, C, D);

fig1h = figure('Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
sopt = sigmaoptions();
sopt.YLim = [-160, 60];
sigmaplot(sysaug, w_span, sopt)
grid on

sysobs = ss(A, H, C, D);
fig2h = figure('Position', [850, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
sopt.YLim = [-80, 60];
sigmaplot(sysobs, w_span, sopt)
grid on

print(fig1h, '-dpdf', 'li5ex3b.pdf')
print(fig2h, '-dpdf', 'li5ex3c.pdf')
