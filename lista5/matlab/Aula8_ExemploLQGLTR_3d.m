%
% Cálculo do compensador LQG/LTR - veja arquivo Aula8_ExemploLQG.doc
%
% SEM 5928 - Sistemas de Controle 
% Escola de Engenharia de São Carlos - USP
%
% Adriano Siqueira - 2016
%

close all
clear
clc

format shorte
fileResults = 'li5ex03pz.txt';
printFlag = true;

%% Declaração de variáveis =====================================================
Nu = 2;
Ny = 2;
Nx = 2;

ro = [1, 1e-1, 1e-3, 1e-6, 1e-9];
num_ro = length(ro);

color_R = interp1(linspace(0, 1, 5), [ 26, 166, 220, 253, 215]/255, linspace(0, 1, num_ro), 'linear');
color_G = interp1(linspace(0, 1, 5), [150, 217, 200, 174,  25]/255, linspace(0, 1, num_ro), 'linear');
color_B = interp1(linspace(0, 1, 5), [ 65, 106, 100,  97,  28]/255, linspace(0, 1, num_ro), 'linear');
color_ro = [color_R; color_G; color_B]';
         
num_samples = 1000;

w_span = logspace(-2, 3, 1000);

Ap = [-3.488,  1.169; ...
     -0.597, -1.847];
Bp = [0.0655, 4.3839; ...
      0.0755, 0.9098];
Cp = [1.0, 0.0;...
      0.0, 1.0];
  
A = [zeros(Nu, Nu+Nx);...
           Bp,    Ap];
B = [eye(Nu); zeros(Nx, Nu)];
C = [zeros(Ny, Nu), Cp];
D = zeros(Ny, Nu);

H = [-4.74880, 33.48100;...
      0.88541, -0.70226;...
      0.90584,  0.00000;...
      0.00000,  0.8344];

%% Análise do sistema em malha aberta ==========================================
compPoles = eig(A);

disp('Open-loop system poles:\n')
disp(compPoles)

%-------------------------------------------------------------------------------
% Figura 1: comparação dos valores singulares em malha aberta para cada ro
fig1h = figure('Name', 'Valores singulares GK', ...
        'Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
ax1h = axes();
set(ax1h, 'XLimMode', 'manual', 'YLimMode', 'manual');
set(ax1h, 'XScale', 'log', 'XLim', [1e-2, 1e3], 'YLim', [-120, 60]);
% Confugure axes tick mark locations
set(ax1h, 'YTickMode', 'manual');
set(ax1h, 'YTick', -120:20:60);
set(ax1h, 'NextPlot', 'add');

%-------------------------------------------------------------------------------
% Figura 2: polos de malha aberta e zeros do compensador
fig2h = figure('Name', 'Polos MA zeros do comp', ...
        'Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
ax2h = axes();
set(ax2h, 'NextPlot', 'add');
plot(real(compPoles), imag(compPoles), 'x', 'Color', [.2, .4, .7])

%-------------------------------------------------------------------------------
% Figura 3: polos de malha fechada
fig3h = figure('Name', 'Polos de malha fechada', ...
        'Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [6, 6], ...
        'PaperPosition', [0, 0, 6, 6], 'PaperPositionMode', 'manual');
ax3h = axes();

%% Projeto LTR =================================================================
for j = 1:num_ro
    Cr=C'*C;
    Br=(B*B')/ro(j);
    Ar=A;
    P=are(Ar,Br,Cr);

    K=B'*P/ro(j);
    A_c=A-B*K-H*C+H*D*K;

    sysG = ss(A, B, C, D);
    sysK = ss(A_c, H, -K, D);
    sysGK = series(sysK,sysG);
    %valores singulares de GK 
    svl = sigma(sysGK, w_span);
    x = [w_span, w_span(end:-1:1)];
    y = 20*log10([svl(2, :), svl(1, end:-1:1)]);
    axes(ax1h);
    patch(x, y, color_ro(j, :), 'FaceAlpha', 0.5, ...
        'EdgeColor', color_ro(j, :));
    
    [compPoles, compZeros] = pzmap(sysK);
    fprintf('Compensator zeros for rho = %e:\n', ro(j))
    disp(compZeros)
    axes(ax2h);
    plot(real(compZeros), imag(compZeros), 'o', 'Color', color_ro(j, :))
    
    sysMF = feedback(sysGK, eye(2));
    [closPoles, closZeros] = pzmap(sysMF);
    fprintf('Closed-loop system poles for rho = %e:\n', ro(j))
    disp(closPoles)
    axes(ax3h)
    plot(real(closPoles), imag(closPoles), 'x', 'Color', [215, 25, 28]/255);
    set(ax3h, 'NextPlot', 'add')
    plot(real(closZeros), imag(closZeros), 'o', 'Color', [ 44,123,182]/255);
    set(ax3h, 'NextPlot', 'replaceChildren')
    text()
    if printFlag
        %print(fig3h, '-dpdf', sprintf('li5ex3f_ro%.2e.pdf', ro(j)))
        print(fig3h, '-dpng', '-r300', regexprep(sprintf('li5ex3f_ro%.2e.png', ro(j)), '(\d)\.(\d\d)', '$1p$2'))
    end
end

% Plotar a malha objetivo
axes(ax1h)
sysobs = ss(A, H, C, D);
svl = sigma(sysobs, w_span);
x = [w_span, w_span(end:-1:1)];
y = 20*log10([svl(2, :), svl(1, end:-1:1)]);
patch(x, y, [0, 0, 0], 'FaceAlpha', 0.5, 'EdgeColor', [0.3, 0.3, 0.3],...
    'LineStyle', '--')
    
set(ax1h, 'NextPlot', 'replaceChildren');
set(ax2h, 'NextPlot', 'replaceChildren');

grid on

axes(ax1h)
legend({'\rho=1', '\rho=1e-1', '\rho=1e-3', '\rho=1e-6', '\rho=1e-9', 'MO'});
xlabel('\omega (rad/s)');
ylabel('\sigma (dB)');

axes(ax2h)
legend({'MA', '\rho=1', '\rho=1e-1', '\rho=1e-3', '\rho=1e-6', '\rho=1e-9'});
xlabel('Real')
ylabel('Complex')

axes(ax3h)
xlabel('Real')
ylabel('Complex')

%% Análise do sistema em malha fechada =========================================
fig4h = figure('Name', 'Valores singulares do sistema a malha fechada', ...
        'Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
ax4h = axes();
set(ax4h, 'XLimMode', 'manual', 'YLimMode', 'manual');
set(ax4h, 'XScale', 'log', 'XLim', [1e-2, 1e3], 'YLim', [-120, 20]);
% Confugure axes tick mark locations
set(ax4h, 'YTickMode', 'manual');
set(ax4h, 'YTick', -120:20:20);
set(ax4h, 'XTickMode', 'manual');
set(ax4h, 'XTick', [1e-2, 1e-1, 1, 1e1, 1e2, 1e3]);
set(ax4h, 'NextPlot', 'add');
sysMF = feedback(sysGK, eye(2));
svl = sigma(sysMF, w_span);

x = [w_span, w_span(end:-1:1)];
y = 20*log10([svl(2, :), svl(1, end:-1:1)]);
patch(x, y, [244, 164, 165]/255, 'EdgeColor', [136, 17, 19]/255)

grid on

if printFlag
    %print(fig1h, '-dpdf', 'li5ex3d.pdf')
    print(fig1h, '-dpng', '-r300', 'li5ex3d.png')
    %print(fig2h, '-dpdf', 'li5ex3e.pdf')
    print(fig2h, '-dpng', '-r300', 'li5ex3e.png')
    %print(fig4h, '-dpdf', 'li5ex3g.pdf')
    print(fig4h, '-dpng', '-r300', 'li5ex3g.png')
end