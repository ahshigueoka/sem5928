clear all;close all;hold off;
%sistema
A=[0 0 0 0 ;
   0 0 0 0 ;
   0.0655 4.3839 -3.488 1.169;
   0.0755 0.9098 -0.597 -1.847];
B=[1 0;
   0 1;
   0 0;
   0 0];
C=[0 0 1 0;
   0 0 0 1];
D=[0 0;0 0];
sysG = ss(A,B,C,D);
%valores singulares do sistema aumentado
w=logspace(-2,3,1000);
sigma(sysG,w);
title('Sistema');

%malha objetivo
H=[-4.7488 33.481;
   0.88541 -0.70226;
   0.90584 0;
   0 0.8344];
sysMO = ss(A,H,C,D);
figure;grid;
%valores singulares da malha objetivo
sigma(sysMO,w);
title('G e Malha objetivo');

%regulador com ro=1 sem utilizar LTRY
ro=(1);
Cr=C'*C;
Br=(1/ro)*B*B';
Ar=A;
K=are(Ar,Br,Cr);
G=(1/ro)*B'*K;
A_1=A-B*G-H*C;
sysK1 = ss(A_1,H,G,D);
z1=tzero(sysK1);
sysGK1 = series(sysK1,sysG);
%valores singulares de GK e MO num mesmo gr�fico
figure;grid;sigma(sysGK1,sysMO,w);
title('GK e MO com ro=1');

%regulador com ro=0.1 sem utilizar LTRY
ro=(0.1);
Cr=C'*C;
Br=(1/ro)*B*B';
Ar=A;
K=are(Ar,Br,Cr);
G=(1/ro)*B'*K;
A_2=A-B*G-H*C;
sysK2 = ss(A_2,H,G,D);
z2=tzero(sysK2);
sysGK2 = series(sysK2,sysG);
%valores singulares de GK e MO num mesmo gr�fico
figure;grid;sigma(sysGK2,sysMO,w);
title('GK e MO com ro=0.1');

%regulador com ro=1e-3 sem utilizar LTRY
ro=(1e-3);
Cr=C'*C;
Br=(1/ro)*B*B';
Ar=A;
K=are(Ar,Br,Cr);
G=(1/ro)*B'*K;
A_3=A-B*G-H*C;
sysK3 = ss(A_3,H,G,D);
z3=tzero(sysK3);
sysGK3= series(sysK3,sysG);
%valores singulares de GK e MO num mesmo gr�fico
figure;grid;sigma(sysGK3,sysMO,w);
title('GK e MO com ro=1e-3');


%regulador com ro=1e-9 sem utilizar LTRY
ro=(1e-9);
Cr=C'*C;
Br=(1/ro)*B*B';
Ar=A;
K=are(Ar,Br,Cr);
G=(1/ro)*B'*K;
A_4=A-B*G-H*C;
sysK4 = ss(A_4,H,G,D);
z4=tzero(sysK4);
sysGK4 = series(sysK4,sysG);
%valores singulares de GK e MO num mesmo gr�fico
figure;grid;sigma(sysGK4,sysMO,w);
title('GK e MO com ro=1e-9 sem LTRY');

% % % % %regulador com ro=1e-9 utilizando LTRY
% % % % figure;
% % % % q=[0];
% % % % %a fun��o LTRY gera o gr�fico de GK mas n�o gera o da MO
% % % % [Af,Bf,Cf,Df,svl]=ltry(A,B,C,D,H,Cr,[ro 0;0 ro],q,w);hold on;sigma(sysMO,w);
% % % % figure;
% % % % semilogx(w,svl);
% % % % 
% % % % tzero(Af,Bf,Cf,Df);
% % % % sysK4f = ss(Af,Bf,Cf,Df);
% % % % z4f=tzero(sysK4f);
% % % % sysGK4f = series(sysK4f,sysG);
% % % % %valores singulares de GK e MO num mesmo gr�fico
% % % % figure;grid;sigma(sysGK4f,sysMO,w);
% % % % title('GK e MO com ro=1e-9 com LTRY');


%p�los e zeros da malha aberta e do compensador
[p0,z0]=pzmap(sysG);
figure;
grid;hold on;
for i=1:4,
   if imag(p0(i))== 0,
      plot([p0(i)],0,'b+');
   else  
      plot(p0(i),'b+');
   end;
end;   
for i=1:2,  
   if imag(z1(i))== 0,
      plot([z1(i)],0,'co');
   else  plot(z1(i),'co');end;
   
   if imag(z2(i))== 0,
      plot([z2(i)],0,'ro');
   else  plot(z2(i),'ro');end;
   
   if imag(z3(i))== 0,
      plot([z3(i)],0,'go');
   else  plot(z3(i),'go');end;
   
   if imag(z4(i))== 0,
      plot([z4(i)],0,'mo');
   else  plot(z4(i),'mo');end;

   
end;	
%polos e zeros da malha fechada para todos os casos
hold off;
sysMF1=feedback(sysGK1,eye(2));
figure;
pzmap(sysMF1);grid;title('p�los e zeros da MF com ro=1');
sysMF2=feedback(sysGK2,eye(2));
figure;
pzmap(sysMF2);grid;title('p�los e zeros da MF com ro=0.1');
sysMF3 = feedback(sysGK3,eye(2));
figure;
pzmap(sysMF3);grid;title('p�los e zeros da MF com ro=1e-3');
sysMF4 = feedback(sysGK4,eye(2));
figure;
pzmap(sysMF4);grid;title('p�los e zeros da MF com ro=1e-9 sem LTRY');
%sysMF4f = feedback(sysGK4f,eye(2));
%figure;
%pzmap(sysMF4f);grid;title('p�los e zeros da MF ro=1e-9 com LTRY');

%valores singulares da malha fechada para ro=1e-9
%sem utilizar LTRY
%figure;grid;sigma(sysMF4,w);
%title('Malha Fechada com ro=1e-9 e sem LTRY');
%utilizando LTRY
%figure;grid;sigma(sysMF4f,w);
%title('Malha Fechada com ro=1e-9 e com LTRY');


