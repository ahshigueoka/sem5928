%
% C�lculo do compensador LQG/LTR - veja arquivo Aula8_ExemploLQG.doc
%
% SEM 5928 - Sistemas de Controle 
% Escola de Engenharia de S�o Carlos - USP
%
% Adriano Siqueira - 2016
%

close all
clear

Nu = 2;
Ny = 2;
Nx = 2;

Ap = [-3.488,  1.169; ...
     -0.597, -1.847];
Bp = [0.0655, 4.3839; ...
      0.0755, 0.9098];
Cp = [1.0, 0.0;...
      0.0, 1.0];
  
A = [zeros(Nu, Nu+Nx);...
           Bp,    Ap];
B = [eye(Nu); zeros(Nx, Nu)];
C = [zeros(Ny, Nu), Cp];
D = zeros(Ny, Nu);

H = [-4.74880, 33.48100;...
      0.88541, -0.70226;...
      0.90584,  0.00000;...
      0.00000,  0.8344];

% Determina��o do regulador linear quadr�tico 
ro=1e-9;
Cr=C'*C;
Br=(B*B')/ro;
Ar=A;
K=are(Ar,Br,Cr);


G=(1/ro)*B'*K;
A_1=A-B*G-H*C;

sysG=ss(A,B,C,D);
sysK = ss(A_1,H,G,D);
sysGK = series(sysK,sysG);

figure()
sigma(sysGK)

%Regulador utilizando LTRY
figure;
q=[0];
%a fun��o LTRY gera o gr�fico de GK mas n�o gera o da MO
[ssKf,svl, w]=ltrsyn(A,B,C,D,H,Cr,[ro 0;0 ro],q);
sysGKf = series(sysKf,sysG);
figure()
grid on
loglog(w, svl);


%Verifica��o do sistema nominal com integradores em malha fechada quanto a barreira de estabilidade
% % figure;
% % semilogx(w,20*log10(inverro),'r');%barreira da estabilidade
% % hold on;grid;
% % semilogx(w,20*log10(CN));%valores singulares da malha fechada
% % title('Sistema nominal com compensador GK em malha fechada e barreira da estabilidade');
% % xlabel('frequ�ncia');
% % ylabel('dB')



