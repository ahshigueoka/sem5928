close all
clear
clc

%% Definition of the transfer functions
num1 = 10*[1, 1];
den1 = conv([1, 5], [1, 10]);

num2 = 10*[1, -1];
den2 = conv([1, 5], [1, 10]);

num3 = 10*[1, 1];
den3 = conv([1, 5], [1, -10]);

TF1 = tf(num1, den1);
TF2 = tf(num2, den2);
TF3 = tf(num3, den3);

num_samples = 1000;
w_vec = logspace(-2, 3, num_samples);
%[mag1, phs1] = bode(TF1);
%bode(TF1, [202, 0, 32]/255, TF2, [64, 64, 64]/255, TF3, [5,113,176]/255);
[m1, p1] = bode(TF1, w_vec);
[m2, p2] = bode(TF2, w_vec);
[m3, p3] = bode(TF3, w_vec);

mdb1 = 20*log10(reshape(m1, 1, num_samples));
mdb2 = 20*log10(reshape(m2, 1, num_samples));
mdb3 = 20*log10(reshape(m3, 1, num_samples));

p1 = reshape(p1, 1, num_samples);
p2 = reshape(p2, 1, num_samples);
p3 = reshape(p3, 1, num_samples);

%% Plot the FRFs of each transfer function
fig1h = figure('Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
    
%-------------------------------------------------------------------------------
% Magnitude of the FRF
ax1h = subplot(2, 1, 1);
% Configure axes limits
set(ax1h, 'XLimMode', 'manual', 'YLimMode', 'manual');
set(ax1h, 'XScale', 'log', 'XLim', [1e-2, 1e3], 'YLim', [-40, 0]);
% Confugure axes tick mark locations
set(ax1h, 'YTickMode', 'manual');
set(ax1h, 'YTick', -40:20:0);

% Hold next plots
set(ax1h, 'NextPlot', 'add');
plot(w_vec, mdb1, 'Color', [202,   0,  32]/255);
plot(w_vec, mdb2, 'Color', [128, 128, 128]/255);
plot(w_vec, mdb3, 'Color', [  5, 113, 176]/255);
set(ax1h, 'NextPlot', 'replaceChildren');

grid on

% Add labels
ylabel('Magnitude (dB)')

%-------------------------------------------------------------------------------
% Phase of the FRF
ax2h = subplot(2, 1, 2);
% Configure axes limits
set(ax2h, 'XLimMode', 'manual', 'YLimMode', 'manual');
set(ax2h, 'XScale', 'log', 'XLim', [1e-2, 1e3], 'YLim', [-180, 180]);
% Confugure axes tick mark locations
set(ax2h, 'YTickMode', 'manual');
set(ax2h, 'YTick', -180:90:180);

% Hold next plots
set(ax2h, 'NextPlot', 'add');
plot(w_vec, p1, 'Color', [202,   0,  32]/255);
plot(w_vec, p2, 'Color', [128, 128, 128]/255);
plot(w_vec, p3, 'Color', [  5, 113, 176]/255);
set(ax2h, 'NextPlot', 'replaceChildren');

grid on

% Add labels
xlabel('\omega (rad/s)')
ylabel('Phase (deg)')

legend('TF1', 'TF2', 'TF3')

%% Plot the step response of the first and the second transfer functions
fig2h = figure('Position', [900, 150, 800, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 6], ...
        'PaperPosition', [0, 0, 12, 6], 'PaperPositionMode', 'manual');

t_vec = linspace(0, 10, num_samples);
y1 = step(TF1, t_vec);
y2 = step(TF2, t_vec);
    
ax3h = axes();
% Configure axes limits
set(ax3h, 'XLimMode', 'manual', 'YLimMode', 'manual');
set(ax3h, 'XLim', [0, 10], 'YLim', [-1, 1]);
% Confugure axes tick mark locations
set(ax3h, 'YTickMode', 'manual');
set(ax3h, 'YTick', [-1, -.5, 0, .5, 1]);
set(ax3h, 'XTickMode', 'manual');
set(ax3h, 'XTick', 0:10);

% Hold next plots
set(ax3h, 'NextPlot', 'add');
plot(t_vec, y1, 'Color', [202,   0,  32]/255);
plot(t_vec, y2, 'Color', [128, 128, 128]/255);
set(ax3h, 'NextPlot', 'replaceChildren');

grid on

% Add labels
xlabel('Time (s)')
ylabel('Y_j')

legend('Y_1', 'Y_2')

%% Save figures to file
%print(fig1h, '-dpdf', 'li5ex01b.pdf')
print(fig1h, '-dpng', '-r300', 'li5ex01b.png');

%print(fig2h, '-dpdf', 'li5ex01c.pdf')
print(fig2h, '-dpng', '-r300', 'li5ex01c.png');