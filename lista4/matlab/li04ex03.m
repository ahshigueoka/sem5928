function G = li04ex03()
    close all

    G = tf(5, conv([1, -.5], conv([1, 2], [1, 2])));
    PM = 12.3;
    IGM = 0.555;
    IGM2 = 2.5;
    
    t = linspace(-pi, pi, 1000);
    
    figh = figure('Position', [50, 50, 800, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [16, 12], ...
        'PaperPosition', [0, 0, 16, 12], 'PaperPositionMode', 'manual');
    axh = axes();
    
    line([-2.5, 0], [0, 0], 'Color', [.7 .0 .0], 'LineWidth', 4)
    
    % Draw the phase margin
    phs = linspace(180, 180+PM, 100);
    patch([0, 0.4*cosd(phs)], [0, 0.4*sind(phs)], [.7 .7 .9], ...
        'EdgeColor', 'none')
    hold on
    
    w = logspace(-3, 2, 100);
    nyquistplot(axh, G, w)
    
    figch = get(figh, 'Children');
    set(figh, 'CurrentAxes', figch(2))
    xlabel('')
    ylabel('')
    
    % Draw the inverse of the gain margin
    plot([-IGM 0], [0 0], 'Color', [.6 .7 .7], 'LineWidth', 2)
    
    % Draw unit circle
    set(figch(2), 'XLim', [-3, 0.1], 'YLim', [-1.2, 1.2]);
    set(figch(2), 'NextPlot', 'add')
    patch(cos(t), sin(t), 'k', 'EdgeColor', [.4 .4 .4], 'FaceColor', 'none')
    line([-cosd(PM), 0], [-sind(PM), 0], 'Color', [.4 .4 .4])
    
    % Add annotations
    text(-IGM, 0.07, 'A')
    text(-IGM2-0.05, 0.07, 'B')
    text(cosd(180+PM)-.07, sind(180+PM)+0.05, 'C')
    text(.4*cosd(180+PM/2), .4*sind(180+PM/2)-0.02, 'MF \rightarrow',...
        'HorizontalAlignment', 'right')
    text(-IGM/2, 0, {'\uparrow', '1/MG'}, ...
        'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', ...
        'interpreter', 'tex')
    plot([-IGM, -IGM2, cosd(180+PM)], ...
         [0, 0, sind(180+PM)], 'ko', 'MarkerFaceColor', [0, 0, 0])
    
    set(figch(2), 'NextPlot', 'replaceChildren');

    print('-dpng', '-r300', 'li04ex03.png')
    print('-dpdf', 'li04ex03.pdf')
end