function L = li04ex02a()
    close all

    K = 30;
    G = tf(1, [1 5 0 0]);
    D = tf(K*[1 0.2], [1 2]);
    L = series(D, G);
    
    figh = figure('Position', [50, 50, 800, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [16, 12], ...
        'PaperPosition', [0, 0, 16, 12], 'PaperPositionMode', 'manual');
    axh = axes();
    bodeplot(axh, L)
    grid on
    
    figch = get(figh, 'Children');
    
    set(figh, 'CurrentAxes', figch(3))
    set(figch(3), 'NextPlot', 'add')
    line([1.99 1.99], [-100 100], 'Color', [.6 .7 .6], 'LineWidth', .5)
    line([2.93 2.93], [-100 -6.06], 'Color', [.7 .6 .6], 'LineWidth', .5)
    
    line([1e-2, 2.93], [-6.05 -6.05], 'Color', [.7 .6 .6], 'LineWidth', .5)
    line([2.93 2.93], [0 -6.05], 'Color', [.5 0 0], 'LineWidth', 1)
    text(2.93, 0, '\leftarrow MG')
    set(figch(3), 'NextPlot', 'replaceChildren');
    
    set(figh, 'CurrentAxes', figch(2))
    set(figch(2), 'NextPlot', 'add')
    line([1.99 1.99], [-162 0], 'Color', [.6 .7 .6], 'LineWidth', .5)
    line([2.93 2.93], [-270 0], 'Color', [.7 .6 .6], 'LineWidth', .5)
    
    line([1e-2, 1.99], [-162.5, -162.5], 'Color', [.6 .7 .6], 'LineWidth', .5)
    line([1.99 1.99], [-180 -162.2], 'Color', [0 .4 0], 'LineWidth', 1)
    text(1.99, -180, 'MF \rightarrow', 'HorizontalAlignment', 'right')
    set(figch(2), 'NextPlot', 'replaceChildren');

    print('-dpng', '-r300', 'li04ex02a.png')
    print('-dpdf', 'li04ex02a.pdf')
end