function L = li04ex02c()
    close all

    K = 110;
    G = tf(1, [1 5 0 0]);
    D = tf(K*[1 0.2], [1 2]);
    L = series(D, G);
    
    t = linspace(-pi, pi, 1000);
    
    figh = figure('Position', [50, 50, 800, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [16, 12], ...
        'PaperPosition', [0, 0, 16, 12], 'PaperPositionMode', 'manual');
    axh = axes();
    
    w = logspace(0, 2, 100);
    nyquistplot(axh, L, w)
    
    figch = get(figh, 'Children');
    
    % Draw unit circle
    set(figch(2), 'XLim', [-3.1, 0.1], 'YLim', [-1.3, 1.3]);
    set(figch(2), 'NextPlot', 'add')
    patch(cos(t), sin(t), 'k', 'EdgeColor', [.4 .4 .4], 'FaceColor', 'none')

    print('-dpng', '-r300', 'li04ex02c.png')
    print('-dpdf', 'li04ex02c.pdf')
end