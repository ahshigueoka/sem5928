function li04ex01()
    figure('Position', [50, 50, 800, 800], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
    axh = axes();
    set(axh, 'XLim', [-4, 4], 'XTick', -4:4, 'XGrid', 'on', ...
             'YLim', [-4, 4], 'YTick', -4:4, 'YGrid', 'on');

    hold on
    plot([-4, 4], [ 0, 0], '-k')
    plot([ 0, 0], [-4, 4], '-k')
    plot(-1, 0, 'x', 'Color', [.7 0 0])
    plot( 2, 0, 'o', 'Color', [.7 0 0])
    hold off

    % Salvar as figuras
    print('-dpng', '-r300', 'li04ex01a.png')
    print('-dpdf', 'li04ex01a.pdf')
end