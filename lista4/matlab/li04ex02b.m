function L = li04ex02b()
    close all

    K = 30;
    G = tf(1, [1 5 0 0]);
    D = tf(K*[1 0.2], [1 2]);
    L = series(D, G);
    PM = 17.8;
    IGM = 0.5;
    
    t = linspace(-pi, pi, 1000);
    
    figh = figure('Position', [50, 50, 800, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [16, 12], ...
        'PaperPosition', [0, 0, 16, 12], 'PaperPositionMode', 'manual');
    axh = axes();
    
    % Draw the phase margin
    phs = linspace(180, 180+PM, 100);
    patch([0, 0.4*cosd(phs)], [0, 0.4*sind(phs)], [.7 .7 .5], ...
        'EdgeColor', [.4 .4 .4])
    hold on
    
    w = logspace(0, 2, 100);
    nyquistplot(axh, L, w)
    
    figch = get(figh, 'Children');
    set(figh, 'CurrentAxes', figch(2))
    
    % Draw unit circle
    set(figch(2), 'XLim', [-1.1, 0.1], 'YLim', [-0.5, 0.1]);
    set(figch(2), 'NextPlot', 'add')
    patch(cos(t), sin(t), 'k', 'EdgeColor', [.4 .4 .4], 'FaceColor', 'none')
    line([-cosd(PM), 0], [-sind(PM), 0], 'Color', [.4 .4 .4])
    
    % Draw the inverse of the gain margin
    line([-IGM 0], [0 0], 'Color', [.7 0 0], 'LineWidth', 2)
    
    % Add annotations
    text(0.4*cosd(180+PM/2), 0.4*sind(180+PM/2), 'MF \rightarrow',...
        'HorizontalAlignment', 'right')
    text(-IGM/2, 0, {'\uparrow', '1/MG'}, ...
        'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', ...
        'interpreter', 'tex')
    
    set(figch(2), 'NextPlot', 'replaceChildren');

    print('-dpng', '-r300', 'li04ex02b.png')
    print('-dpdf', 'li04ex02b.pdf')
end