function L = li04ex02d()
    close all

    K = 30;
    G = tf(1, [1 5 0 0]);
    D = tf(K*[1 0.2], [1 2]);
    L = series(D, G);
    
    %% Find the frequency where the margin phase is maximum
    num_samples = 1e3;
    w_vec = logspace(-2, 1, num_samples);
    FRF_L = reshape(freqresp(L, w_vec), [1, num_samples]);
    FRF_Lmag = abs(FRF_L);
    FRF_Lphs = 180/pi*unwrap(angle(FRF_L));
    
    [PMmax, ind_PMmax] = max(FRF_Lphs);
    w_PMmax = w_vec(ind_PMmax);
    GMmax = 20*log10(FRF_Lmag(ind_PMmax));
    
    figh = figure('Position', [50, 50, 800, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [16, 12], ...
        'PaperPosition', [0, 0, 16, 12], 'PaperPositionMode', 'manual');
    axh = axes();
    bodeplot(axh, L)
    grid on
    
    figch = get(figh, 'Children');
    
    set(figh, 'CurrentAxes', figch(3))
    set(figch(3), 'NextPlot', 'add')
    line([w_PMmax w_PMmax], [-150, 0], 'Color', [.7 .6 .6], 'Linewidth', .5)
    line([1e-2, w_PMmax], [GMmax GMmax], 'Color', [.7 .6 .6], 'LineWidth', .5)
    line([w_PMmax w_PMmax], [0 GMmax], 'Color', [.5 0 0], 'LineWidth', 1)
    text(w_PMmax, GMmax/2, '\leftarrow MG')
    set(figch(3), 'NextPlot', 'replaceChildren');
    
    set(figh, 'CurrentAxes', figch(2))
    set(figch(2), 'NextPlot', 'add')
    line([w_PMmax w_PMmax], [-180 0], 'Color', [.6 .7 .6], 'LineWidth', .5)
    line([1e-2, w_PMmax], [PMmax, PMmax], 'Color', [.6 .7 .6], 'LineWidth', .5)
    line([w_PMmax w_PMmax], [-180 PMmax], 'Color', [0 .4 0], 'LineWidth', 1)
    text(w_PMmax, (PMmax-180)/2, '\leftarrow max MF')
    set(figch(2), 'NextPlot', 'replaceChildren');
    
    fprintf('For maximum phase margin:\n')
    fprintf(' w: %e rad/s\n', w_PMmax)
    fprintf('PM: %e deg\n', 180+PMmax)
    fprintf('GM: %e dB\n', -GMmax)

    print('-dpng', '-r300', 'li04ex02d.png')
    print('-dpdf', 'li04ex02d.pdf')
end