function li03ex03c()
%LI03EX03C SEM5928, Lista 3, Exr. 03.c)
    close all
    % Parâmetros do sistema
    I1 = 1;
    I2 = 0.1;
    bint = 1:5;
    bvec = 0.001*bint;
    kint = 1:3;
    kvec = 0.1*kint;
    blen = length(bvec);
    klen = length(kvec);
    
    % Tempo de simulação
    tfinal = 50;
    
    % Configurar a janela de plotagem
    figh = figure('Position', [50, 50, 800, 400], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 6], ...
        'PaperPosition', [0, 0, 12, 6], 'PaperPositionMode', 'manual');
    axh = axes();
    set(axh, 'XLim', [0, tfinal], 'XGrid', 'on', 'YGrid', 'on');
    
    % Calcular a resposta no tempo para cada combinação b e k
    for ib = 1:blen
        for ik = 1:klen
            b = bvec(ib);
            k = kvec(ik);
            
            % Função transferência Tc -> Theta2
            num2 = [b, k];
            den = conv([I1 b k], [I2 b k]) - [0 0 b^2, 2*b*k, k^2];
            TF2 = tf(num2, den);
            
            % Controlador notch
            K = 2.6331;
            numN = conv([2, 1], [.5929, 0, 1]);
            denN = conv([.05, 1], [.05, 1]);
            N = tf(K*numN, denN);
            
            % Entrada do sistema
            t = linspace(0, tfinal, 100001);
            u = ones(size(t));
            
            % Resposta no tempo para FT 2
            L = series(N, TF2);
            MF = feedback(L, 1);
            theta2 = lsim(MF, u, t);
            
            figure(figh)
            % Plotar as respostas no tempo
            cla(axh)
            set(axh, 'NextPlot', 'add')
            plot(axh, [0, tfinal], [1, 1], '--k')
            plot(axh, t, theta2, 'Color', [ 90, 180, 172]/255);
            xlabel('Tempo (s)')
            ylabel('\theta_2')
            title(sprintf('Sistema a malha fechada, b=%f, k=%f', b, k))
            
            % Salvar as figuras
            print('-dpng', '-r300', sprintf('lista3exr3i-MFb%dk%d.png', bint(ib), kint(ik)))
            print('-dpdf', sprintf('lista3exr3i-MFb%dk%d.pdf', bint(ib), kint(ik)))
        end
    end
            
end

