function li3ex2a()
    tfinal = 5;
    t = linspace(0, tfinal, 5001);
    
    r = sin(2*pi*0.1*t) + 0.5*sin(2*pi*t) + 0.2*sin(2*pi*10*t);
    y = sin(2*pi*0.1*t) + 0.5*sin(2*pi*t+10/180*pi) ...
        + 10^(-7/20)*0.2*sin(2*pi*10*t+60/180*pi);
    
    figh = figure('Position', [50, 50, 800, 400], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 6], ...
        'PaperPosition', [0, 0, 12, 6], 'PaperPositionMode', 'manual');
    axh = axes();
    set(axh, 'XLim', [0, tfinal], 'YLim', [-2, 40], ...
                'XGrid', 'on', 'YGrid', 'on');
            
    plot(t, r, 'Color', [0.3, 0.3, .7])
    hold on
    plot(t, y, 'Color', [0.2, .7, 0.2])
            
    xlabel('Tempo (s)')
    ylabel('Magnitude')
    legend({'r(t)', 'y(t)'}, 'Location', 'NorthEast')
    set(axh, 'NextPlot', 'replaceChildren');
    title('Lista 3, exr. 2.a)')

    % Salvar as figuras
    print('-dpng', '-r300', 'lista3exr2a.png')
    print('-dpdf', 'lista3exr2a.pdf')
end