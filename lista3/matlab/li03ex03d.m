close all
clear
clc

% Parâmetros do sistema
I1 = 1;
I2 = 0.1;
b = 0.003;
k = 0.2;

% Função transferência Tc -> Theta1
num1 = [I2, b, k];
den = conv([I1 b k], [I2 b k]) - [0 0 b^2, 2*b*k, k^2];
TF1 = tf(num1, den);

% Função transferência Tc -> Theta2
num2 = [b, k];
TF2 = tf(num2, den);

% Projeto do sistema de controle no sisotool
sisotool(TF2)