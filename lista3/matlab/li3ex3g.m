function li3ex3g()
    a = 1;
    c = 20;
    
    num = [1, 0, a];
    den = [1, 2*c, c];
    
    N = tf(num, den);
    
    wvec = logspace(-2, 1, 1000);
    num_samples = length(wvec);
    
    FRFN = freqresp(N, wvec, 'Hz');
    FRFN_mag = 20*log10(abs(reshape(FRFN, 1, num_samples)));
    FRFN_phs = 180/pi*angle(reshape(FRFN, 1, num_samples));
    
    figh = figure('Position', [50, 50, 800, 400], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 12], ...
        'PaperPosition', [0, 0, 12, 12], 'PaperPositionMode', 'manual');
    
    ax1h = subplot(2, 1, 1);
    plot(wvec, FRFN_mag)
    set(ax1h, 'YLim', [-100, 0], 'YTickMode', 'manual',...
        'YTick', -100:20:0, ...
        'XGrid', 'on', 'YGrid', 'on', 'XScale', 'log');
    grid on
    ylabel('|N(\omega)| (dB)')
    title('FRF do filtro Notch')
    
    ax2h = subplot(2, 1, 2);
    plot(wvec, FRFN_phs)
    set(ax2h, 'YLim', [-180, 180], ...
        'YTickMode', 'manual', 'YTick', -180:90:180, ...
        'XGrid', 'on', 'YGrid', 'on', 'XScale', 'log');
    grid on
    xlabel('\omega (Hz)')
    ylabel('\angle N(\omega)')
  
    print('-dpng', '-r300', 'lista3exr3g.png')
    print('-dpdf', 'lista3exr3g.pdf')
end