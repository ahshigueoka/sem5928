function li03ex03c()
%LI03EX03C SEM5928, Lista 3, Exr. 03.c)
    close all
    % Parâmetros do sistema
    I1 = 1;
    I2 = 0.1;
    bint = 1:5;
    bvec = 0.001*bint;
    kint = 1:3;
    kvec = 0.1*kint;
    blen = length(bvec);
    klen = length(kvec);
    
    % Tempo de simulação
    tfinal = 50;
    
    % Configurar a janela de plotagem
    figh = figure('Position', [50, 50, 800, 400], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12, 6], ...
        'PaperPosition', [0, 0, 12, 6], 'PaperPositionMode', 'manual');
    axh = axes();
    set(axh, 'XLim', [0, tfinal], 'YLim', [-2, 40], ...
                'XGrid', 'on', 'YGrid', 'on');
    
    % Calcular a resposta no tempo para cada combinação b e k
    for ib = 1:blen
        for ik = 1:klen
            b = bvec(ib);
            k = kvec(ik);
            
            % Função transferência Tc -> Theta1
            num1 = [I2, b, k];
            den = conv([I1 b k], [I2 b k]) - [0 0 b^2, 2*b*k, k^2];
            TF1 = tf(num1, den);
    
            % Função transferência Tc -> Theta2
            num2 = [b, k];
            TF2 = tf(num2, den);
            
            % Entrada do sistema
            t = linspace(0, tfinal, 100001);
            u = sin(1.48*t);
            
            % Resposta no tempo para a FT 1
            theta1 = lsim(TF1, u, t);
            
            % Resposta no tempo para FT 2
            theta2 = lsim(TF2, u, t);
            
%             A = [    0,     0,     1,     0;...
%                      0,     0,     0,     1;...
%                  -k/I1,  k/I1, -b/I1,  b/I1;...
%                   k/I2, -k/I2,  b/I2, -b/I2];
%             B = [0; 0; 1; 0];
%             C = [0, 0, 1, 0; 0, 0, 0, 1];
%             D = [0; 0];
%             sys = ss(A, B, C, D);
%             
%             theta = lsim(sys, u, t, [0; 0; 0; 0]);
            
            figure(figh)
            cla(axh)
            set(axh, 'NextPlot', 'add');
            % Plotar as respostas no tempo
            plot(axh, t, theta1, 'Color', [216, 179, 101]/255);
            plot(axh, t, theta2, 'Color', [ 90, 180, 172]/255);
            xlabel('Tempo (s)')
            ylabel('\theta_k')
            legend({'k=1', 'k=2'}, 'Location', 'SouthEast')
            set(axh, 'NextPlot', 'replaceChildren');
            title(sprintf('Resposta no tempo, b=%f, k=%f', b, k))
            
            % Salvar as figuras
            print('-dpng', '-r300', sprintf('lista3exr3c-b%dk%d.png', bint(ib), kint(ik)))
            print('-dpdf', sprintf('lista3exr3c-b%dk%d.pdf', bint(ib), kint(ik)))
        end
    end
            
end

