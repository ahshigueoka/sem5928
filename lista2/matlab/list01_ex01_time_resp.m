function [t, x] = list01_ex01_time_resp(param)
    opt = odeset('RelTol', 1e-3, 'AbsTol', 1e-5, ...
        'Vectorized', 'on', 'MaxStep', 1e-2);
    
    [A, B, C, D] = list01_ex01_mat(param);
    
    fun = @(t, x) list01_ex01_odefun(t, x, A, B, param.u);
    [t, x] = ode45(fun, param.t_span, param.x_0, opt);
end