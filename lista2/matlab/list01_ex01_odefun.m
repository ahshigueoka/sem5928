function dx_dt = list01_ex01_odefun(t, x, A, B, u)
    dx_dt = A*x + repmat(B*u(t), 1, size(x, 2));
end