function [A, B, C, D] = list01_ex01_mat(param)
    k = param.k;
    k_t = param.k_t;
    b = param.b;
    M = param.M;
    m = param.m;
    m_c = param.m_c;
    L = param.L;
    g = param.g;
    L_c = param.L_c;
    I_c = param.I_c;
    den = (M+m+m_c)*((m+m_c)*L_c^2+I_c)-(m+m_c)^2;
    
    A = zeros(4);
    A(1:2, 3:4) = eye(2);
    A(3, 1) = -k*((m+m_c)*L_c^2+I_c)/den;
    A(3, 2) = -(m+m_c)*((m+m_c)*g*L_c-k_t)/den;
    A(3, 3) = -b*((m+m_c)*L_c^2+I_c)/den;
    A(4, 1) = k*(m+m_c)/den;
    A(4, 2) = (M+m+m_c)*((m+m_c)*g*L_c-k_t)/den;
    A(4, 3) = b*(m+m_c)/den;
    
    B = zeros(4, 1);
    B(3) = ((m+m_c)*L_c^2+I_c)/den;
    B(4) = -(m+m_c)/den;
    
    C = eye(2, 4);
    
    D = zeros(2, 1);
end