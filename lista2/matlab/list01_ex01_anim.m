function Frame = list01_ex01_anim(param, t, x)
    num_frames = length(t);
    Frame(num_frames) = struct('cdata',[],'colormap',[]);
    
    figure
    for f_it = 1:num_frames
        fill([-.05 .05 .05 -.05] + x(f_it, 1), [-.05 -.05 .05 .05], [.5 .5 .5])
        set(gca,'NextPlot','add');
        x_pin = x(f_it, 1);
        y_pin = 0;
        x_m_c = x(f_it, 1) + param.L_c*sin(x(f_it, 2));
        y_m_c = param.L_c*cos(x(f_it, 2));
        plot([x_pin, x_m_c], [y_pin, y_m_c], '-bo')
        axis equal
        axis([-.5 .5 -.1 .3]);
        Frame(f_it) = getframe;
        set(gca,'NextPlot','replaceChildren');
    end
end