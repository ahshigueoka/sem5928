close all
clear
clc

param.k = 10;
param.k_t = 1;
param.b = .5;
param.M = 10;
param.m = 0;
param.m_c = .3;
param.L = .2;
param.L_c = (param.m/param.m_c/2+1)/(param.m/param.m_c+1)*param.L;
param.I_c = param.m*param.L^2/12 + ...
    param.m*(param.L_c-param.L/2)^2 + param.m_c*(param.L-param.L_c)^2;
param.g = 9.80665;
param.t_span = 0:1e-3:300;
param.x_0 = [0 0 0 0]';
param.u = @(t) 0;

[A, B, C, D] = list01_ex01_mat(param);
sys_ss = ss(A, B, C, D);
w = logspace(-1, 2, 1e4);

figure
bode(sys_ss, w)

figure
nyquist(sys_ss, w)

% Time response for a w < w_n1
w_u = 100;
param.u = @(t) heaviside(t);
[t, x] = list01_ex01_time_resp(param);
figure
subplot(2, 1, 1)
plot(t, x(:, 1))
grid on
title(sprintf('Position of the block, w_u = %f', w_u))
subplot(2, 1, 2)
plot(t, x(:, 2))
grid on
title(sprintf('Angle of the pendulum, w_u = %f', w_u))